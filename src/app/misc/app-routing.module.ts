import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { EditorContainerComponent } from '@components/editor-container/editor-container.component';
import { WelcomeComponent } from '@components/welcome/welcome.component';
import { EditGuard } from '@misc/edit.guard';

const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'edit', component: EditorContainerComponent, canActivate: [EditGuard] }
]; // sets up routes constant where you define your routes

// configures NgModule imports and exports
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
