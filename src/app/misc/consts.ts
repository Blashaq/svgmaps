export const svgNamespace = 'http://www.w3.org/2000/svg'
export const snackMessages = {
    clickInsidePlease: `Please select points inside selected path.
  If you wish to finish drawing, press Enter.`,
    pan: 'Drag map to change its position on the screen.',
    select: 'Select a path on map. When you choose path, you can split it using "Split selected element" option',
    edit: 'Click on any vertice of selected path. Then drag it around.',
    split: 'Start drawing a path inside selected element. When path is finished, press Enter.',
    wrongFragment: `You cannot click outside of shape borders.`,
    selected: 'You can now edit or split selected path.',
    finishedSplitting: 'Done!'
};