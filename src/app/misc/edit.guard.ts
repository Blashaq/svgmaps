import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { EditorStateService } from '@app/services/editor-state/editor-state.service';

@Injectable({ providedIn: 'root' })
export class EditGuard implements CanActivate {
    _canActivate = false;
    constructor(
        private router: Router,
        editorStateService: EditorStateService
    ) {
        editorStateService.svg.subscribe(svg => {
            this._canActivate = svg !== undefined;
        })
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
        if (this._canActivate) {
            return this._canActivate;
        }
        else {
            this.router.navigate(['']);
        }
    }
}
