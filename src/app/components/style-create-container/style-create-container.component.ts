import { Component, OnInit } from '@angular/core';
import { ElementStyle } from '@app/services/svg-style/styleUtils';
import { SvgStyleService } from '@services/svg-style/svg-style.service';




@Component({
  selector: 'app-style-create-container',
  templateUrl: './style-create-container.component.html',
  styleUrls: ['./style-create-container.component.css']
})
export class StyleCreateContainerComponent implements OnInit {

  styles: ElementStyle[] = []

  newStyle: ElementStyle = undefined

  constructor(private styleService: SvgStyleService) { }

  onAddNewStyle() {
    this.newStyle = { name: "changeMe", border: "dotted", borderColor: "#ffffff", borderWidth: 1, fillColor: "#ffffff" }
  }
  clearNewStyle() {
    this.newStyle = undefined
  }

  ngOnInit(): void {
    this.styleService.styles.subscribe({
      next: styles => {
        this.styles = [...styles.values()]
        this.newStyle = undefined
      }
    })
  }

}
