import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCreateContainerComponent } from './style-create-container.component';

describe('StyleCreateContainerComponent', () => {
  let component: StyleCreateContainerComponent;
  let fixture: ComponentFixture<StyleCreateContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StyleCreateContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCreateContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
