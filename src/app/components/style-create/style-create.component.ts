import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogDelete } from '@app/dialogs/delete/dialog-delete';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ElementStyle, strokeTypes } from '@app/services/svg-style/styleUtils';
import { SvgStyleService } from '@app/services/svg-style/svg-style.service';
import { interval } from 'rxjs';
import { debounce } from 'rxjs/operators'

@Component({
  selector: 'app-style-create',
  templateUrl: './style-create.component.html',
  styleUrls: ['./style-create.component.css']
})
export class StyleCreateComponent implements OnInit {

  borderStyles = strokeTypes

  @Input("style")
  style: ElementStyle

  @Input("editableName")
  unsavedStyle: boolean

  styleForm: FormGroup

  constructor(private svgStyleService: SvgStyleService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) {
    this.styleForm = new FormGroup({
      name: new FormControl('', [Validators.required, this.nameTakenValidator(), this.nameWithWhitespacesValidator()]),
      fillColor: new FormControl('', Validators.required),
      border: new FormControl('', Validators.required),
      borderWidth: new FormControl('', Validators.required),
      borderColor: new FormControl('', Validators.required)
    })
  }
  onSubmit() {

  }

  save() {
    const nameForm = this.styleForm.get('name')
    if (nameForm.errors) {
      this.snackBar.open("Style name contains errors.", null, { duration: 5000 })
      return
    }
    const style: ElementStyle = {
      name: this.styleForm.get('name').value,
      border: this.styleForm.get('border').value,
      borderColor: this.styleForm.get('borderColor').value,
      borderWidth: this.styleForm.get('borderWidth').value,
      fillColor: this.styleForm.get('fillColor').value
    }
    this.svgStyleService.saveStyle(style)
  }

  deleteStyle() {
    const dialogRef = this.dialog.open(DialogDelete, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.svgStyleService.delete(this.style)
      }
    });
  }

  ngOnInit(): void {
    const nameControl = this.styleForm.get('name')
    nameControl.setValue(this.style.name)
    // if (!this.unsavedStyle) {
    //   nameControl.disable({ emitEvent: false })
    // }
    this.styleForm.get('fillColor').setValue(this.style.fillColor)
    this.styleForm.get('border').setValue(this.style.border)
    this.styleForm.get('borderWidth').setValue(this.style.borderWidth)
    this.styleForm.get('borderColor').setValue(this.style.borderColor)
    if (!this.unsavedStyle) {
      this.styleForm.valueChanges.pipe(debounce(() => interval(100))).subscribe({
        next: (style: ElementStyle) => {
          this.svgStyleService.updateStyle(style)
        }
      })
    }
  }

  nameTakenValidator(): ValidatorFn {
    return (control): ValidationErrors | null => {
      const nameExists = this.svgStyleService.hasStyle(control.value)
      return nameExists && this.unsavedStyle ? { forbiddenName: { value: control.value } } : null
    }
  }

  nameWithWhitespacesValidator(): ValidatorFn {
    return (control): ValidationErrors | null => {
      return /\s/g.test(control.value) ? { whitespacedName: { value: control.value } } : null
    }
  }

}

