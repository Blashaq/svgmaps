import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCreateComponent } from './style-create.component';

describe('StyleCreateComponent', () => {
  let component: StyleCreateComponent;
  let fixture: ComponentFixture<StyleCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StyleCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
