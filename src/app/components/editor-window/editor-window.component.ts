import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild, } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { snackMessages } from '@app/misc/consts';
import { SvgEditService } from '@app/services/svg-edit/svg-edit.service';
import { PartialObserver } from 'rxjs';
import { EditorStateService, Mode } from 'src/app/services/editor-state/editor-state.service';
import { EditableSVG, Point } from '@app/services/svg-edit/editableSVG';

const snackDuration = 5000;

@Component({
  selector: 'app-editor-window',
  templateUrl: './editor-window.component.html',
  styleUrls: ['./editor-window.component.css']
})
export class EditorWindowComponent implements OnInit, AfterViewInit {
  @ViewChild('svgContainer') svgContainer: ElementRef<Element>;

  mode: Mode;

  // pan parameters
  isMousePressed = false;
  panStartPoint: Point;

  constructor(private svgEditService: SvgEditService,
    private editorState: EditorStateService,
    private snackBar: MatSnackBar) { }

  @HostListener('click', ['$event'])
  handleShapeClick(event: MouseEvent): void {
    switch (this.mode) {
      case Mode.select: {
        this.handleSelectClick(event);
        break;
      }
      case Mode.split: {
        this.handleSplitClick(event);
        break;
      }
    }
  }

  @HostListener('mousedown', ['$event'])
  moveDown(event: MouseEvent): void {
    this.isMousePressed = true;
    switch (this.mode) {
      case Mode.pan: {
        this.handleStartPanning(event);
        break;
      }
    }
  }

  private handleStartPanning(event: MouseEvent): void {
    this.panStartPoint = { x: event.x, y: event.y };
  }

  @HostListener('mousemove', ['$event'])
  mouseMove(event: MouseEvent): void {
    switch (this.mode) {
      case Mode.pan: {
        if (this.isMousePressed) {
          this.handlePan(event);
        }
        break;
      }
    }
  }

  private handlePan(event: MouseEvent): void {
    const panEndPoint = { x: event.x, y: event.y };
    this.svgEditService.display.pan(this.panStartPoint, panEndPoint, false);
  }

  @HostListener('mouseup', ['$event'])
  mouseUp(event: MouseEvent): void {
    this.isMousePressed = false;
    switch (this.mode) {
      case Mode.pan: {
        const panEndPoint = { x: event.x, y: event.y };
        this.svgEditService.display.pan(this.panStartPoint, panEndPoint, true);
        break;
      }
    }
  }

  @HostListener('document:keydown.enter', ['$event'])
  enterPressed(event: KeyboardEvent): void {
    if (this.mode === Mode.split) {
      this.svgEditService.editorFunctions.splitSelectedPath();
      this.editorState.reset();
      this.snackBar.open(snackMessages.finishedSplitting, null, { duration: snackDuration });
    }
    this.mode = Mode.select;
  }

  private handleSplitClick(event: MouseEvent): void {
    const svgP = this.svgEditService.editorFunctions.getClickCoordinates({ x: event.clientX, y: event.clientY });
    const result = this.svgEditService.editorFunctions.drawLine(svgP)
    if (!result.success) {
      this.snackBar.open(snackMessages.wrongFragment, null, { duration: snackDuration });
    }
  }

  private handleSelectClick(event: MouseEvent): void {
    const selected = event.target;
    if (selected instanceof SVGGeometryElement) {
      const editedElement = this.svgEditService.editorFunctions.selectElement(selected);
      if (editedElement) {
        this.editorState.elementSelected.next(true);
      }

      this.snackBar.open(snackMessages.selected, null, { duration: snackDuration });
    }
    else {
      this.svgEditService.editorFunctions.unselectElement()
      this.editorState.elementSelected.next(false)
    }
  }

  ngAfterViewInit(): void {
    this.svgContainer.nativeElement
    this.svgEditService.editedFile.subscribe(this.svgObserver);
    this.editorState.scale.subscribe(this.zoomObserver);
    this.editorState.mode.subscribe(this.editorModeObserver);
    this.editorState.previewMode.subscribe(this.styleObserver)
  }

  ngOnInit(): void {

  }

  svgObserver: PartialObserver<EditableSVG> = {
    next: svg => {
      if (svg) {
        this.svgContainer.nativeElement.innerHTML = '';
        const initialViewBox = {
          x: 0,
          y: 0,
          w: this.svgContainer.nativeElement.clientWidth,
          h: this.svgContainer.nativeElement.clientHeight
        };
        this.svgEditService.display.setInitialViewbox(initialViewBox)
        this.svgContainer.nativeElement.appendChild(svg.svgImage);
      }
    }
  }

  zoomObserver: PartialObserver<number> = {
    next: (zoom) => {
      this.svgEditService.display.setZoom(1 / zoom);
    }
  }
  editorModeObserver: PartialObserver<Mode> = {
    next: value => {
      switch (value) {
        case Mode.split:
          {
            this.snackBar.open(snackMessages.split, null, { duration: snackDuration });
            break;
          }
        case Mode.pan: {
          this.snackBar.open(snackMessages.pan, null, { duration: snackDuration });
          break;
        }
        case Mode.select: {
          this.snackBar.open(snackMessages.select, null, { duration: snackDuration });
          break;
        }
      }
      if (this.mode === Mode.split) {
        this.svgEditService.editorFunctions.removeLine()
      }
      this.mode = value;
    }
  }

  styleObserver: PartialObserver<boolean> = {
    next: value => {
      if (value) {
        this.svgEditService.display.previewStyle();
      }
      else {
        this.svgEditService.display.editStyle();
      }
    }
  }
}

