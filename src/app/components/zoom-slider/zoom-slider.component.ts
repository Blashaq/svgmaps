import { Component, OnInit } from '@angular/core';
import { EditorStateService } from 'src/app/services/editor-state/editor-state.service';

@Component({
  selector: 'app-zoom-slider',
  templateUrl: './zoom-slider.component.html',
  styleUrls: ['./zoom-slider.component.css']
})
export class ZoomSliderComponent implements OnInit {

  max = 3;
  min = 0.1;
  step = 0.1;
  thumbLabel = true;
  tickInterval = 'auto';
  value = 1;

  constructor(private editorState: EditorStateService) { }

  formatLabel(value: number): string {
    return `${(value * 100).toFixed(0)}%`;
  }
  zoomChanged(value: number): void {
    this.editorState.setZoom(value);
  }

  ngOnInit(): void {
  }

}
