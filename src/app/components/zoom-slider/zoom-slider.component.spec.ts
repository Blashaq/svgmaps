import { EditorStateService } from '@app/services/editor-state/editor-state.service';
import { ZoomSliderComponent } from './zoom-slider.component';

describe('ZoomSliderComponent', () => {
  let component: ZoomSliderComponent;
  let editorStateSpy: EditorStateService


  beforeEach(() => {
    editorStateSpy = jasmine.createSpyObj('EditorStateService', ['setZoom'])
    component = new ZoomSliderComponent(editorStateSpy)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should notify editorStateService that zoom changed', () => {
    component.zoomChanged(1)
    expect(editorStateSpy.setZoom).toHaveBeenCalled()
  })
});
