import { MatDialog } from '@angular/material/dialog';

import { MenuFileComponent } from './menu-file.component';
import { OpenFileSelectOption } from '@dialogs/dialog-content-open-file/dialog-content-open-file.component';
import * as rxjs from 'rxjs';
import { SvgFileService } from '@app/services/svg-file/svg-file.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EditorStateService } from '@app/services/editor-state/editor-state.service';

describe('MenuFileComponent', () => {
  let component: MenuFileComponent;
  let matDialogSpy: jasmine.SpyObj<MatDialog>
  let svgFileServiceSpy: jasmine.SpyObj<SvgFileService>
  let editorStateServiceSpy: jasmine.SpyObj<EditorStateService>
  let httpSpy: jasmine.SpyObj<HttpClient>
  let routerSpy: jasmine.SpyObj<Router>

  beforeEach(() => {
    matDialogSpy = jasmine.createSpyObj<MatDialog>('MatDialog', ['open'])
    svgFileServiceSpy = jasmine.createSpyObj('SvgFileService', { 'loadSvg': Promise.resolve(undefined), 'svg': rxjs.of(undefined) })
    editorStateServiceSpy = jasmine.createSpyObj<EditorStateService>('EditorStateService', {}, { 'svg': new rxjs.ReplaySubject(1) })
    httpSpy = jasmine.createSpyObj('HttpClient', { 'get': rxjs.of(new ArrayBuffer(5)) })
    routerSpy = jasmine.createSpyObj('Router', ['navigate'])
    component = new MenuFileComponent(matDialogSpy, svgFileServiceSpy, editorStateServiceSpy, httpSpy, routerSpy)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call router redirect when file is loaded', async () => {
    const option: OpenFileSelectOption = {
      name: "somefile.svg",
      path: "/path.svg"
    }
    await component.handleDialogResult(option)
    expect(httpSpy.get).toHaveBeenCalledOnceWith(option.path, jasmine.any(Object))
    expect(routerSpy.navigate).toHaveBeenCalled()
  })
  it('should not redirect if nothing was selected', async () => {
    const option: OpenFileSelectOption = undefined
    await component.handleDialogResult(option)
    expect(httpSpy.get).not.toHaveBeenCalled()
    expect(routerSpy.navigate).not.toHaveBeenCalled()
  })
});
