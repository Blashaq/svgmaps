import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { SvgFileService } from 'src/app/services/svg-file/svg-file.service';
import { DialogContentOpenFileComponent, OpenFileSelectOption } from '@app/dialogs/dialog-content-open-file/dialog-content-open-file.component';
import { EditorStateService } from '@app/services/editor-state/editor-state.service';

@Component({
  selector: 'app-menu-file',
  templateUrl: './menu-file.component.html',
  styleUrls: ['./menu-file.component.css']
})
export class MenuFileComponent implements OnInit {

  @ViewChild('fileSelectInputDialog')
  fileSelectInputDialog: ElementRef<HTMLInputElement>

  private _canSave = false;

  constructor(
    public dialog: MatDialog,
    public svgFileService: SvgFileService,
    public editorStateService: EditorStateService,
    private http: HttpClient,
    private router: Router) {
    editorStateService.svg.subscribe(value => {
      if (value) {
        this._canSave = true;
      } else {
        this._canSave = false
      }
    })
  }

  openFileDialog() {
    const dialogRef: MatDialogRef<DialogContentOpenFileComponent, OpenFileSelectOption> = this.dialog.open(DialogContentOpenFileComponent);
    dialogRef.afterClosed().subscribe(option => {
      this.handleDialogResult(option)
    });
  }

  async handleDialogResult(option: OpenFileSelectOption) {
    if (!option) {
      return
    }
    if (!option.isUpload && option.path) {
      const data = await this.http.get(option.path, { responseType: 'arraybuffer' }).toPromise()
      await this.activateEdit(data)
    }
    else if (option.isUpload) {
      this.uploadFileDialog()
    }
  }

  async uploadFileDialog() {
    const e: HTMLElement = this.fileSelectInputDialog.nativeElement;
    e.click();
  }

  handleFileUpload(files: File[]) {
    if (files.length === 0) {
      return
    }
    const file = files[0]
    file.arrayBuffer().then(file => this.activateEdit(file))

  }

  private async activateEdit(fileContents: ArrayBuffer) {
    await this.svgFileService.loadSvg(fileContents)
    await this.router.navigate(['/edit/'])
  }

  canSave(): boolean {
    return this._canSave;
  }

  save(fileName = 'map.svg'): void {
    this.editorStateService.previewStyle(true);

    const svg = this.svgFileService.getSerializedSvg();
    const element = document.createElement('a');
    const fileType = 'text/xml';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(svg)}`);
    element.setAttribute('download', fileName);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }

  ngOnInit(): void {
  }
}
