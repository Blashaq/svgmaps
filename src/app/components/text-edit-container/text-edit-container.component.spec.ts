import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextEditContainerComponent } from './text-edit-container.component';

describe('TextEditContainerComponent', () => {
  let component: TextEditContainerComponent;
  let fixture: ComponentFixture<TextEditContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextEditContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextEditContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
