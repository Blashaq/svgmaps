import { Component, OnInit } from '@angular/core';
import { TextData } from '@app/services/svg-text/textUtils';
import { SvgTextService } from '@services/svg-text/svg-text.service';

@Component({
  selector: 'app-text-edit-container',
  templateUrl: './text-edit-container.component.html',
  styleUrls: ['./text-edit-container.component.css']
})
export class TextEditContainerComponent implements OnInit {

  textElements: TextData[] = [];
  called: boolean = false

  constructor(private svgTextService: SvgTextService) { }

  ngOnInit(): void {
    this.svgTextService.textElements.subscribe({
      next: textElements => {
        this.textElements = textElements;
        this.called = true;
      }
    })
  }

  onAddText() {
    this.svgTextService.addText()
  }

}
