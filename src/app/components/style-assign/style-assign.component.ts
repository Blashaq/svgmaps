import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SvgEditService } from '@app/services/svg-edit/svg-edit.service';
import { SvgStyleService } from '@app/services/svg-style/svg-style.service';

@Component({
  selector: 'app-style-assign',
  templateUrl: './style-assign.component.html',
  styleUrls: ['./style-assign.component.css']
})
export class StyleAssignComponent implements OnInit {

  styleNames: string[] = []
  selectedElementStyleName = new FormControl('')
  selectedElement: SVGGeometryElement;

  constructor(private svgStyleService: SvgStyleService,
    private svgEditService: SvgEditService) {
    this.svgStyleService.styles.subscribe({
      next: styles => {
        this.styleNames = Array.from(styles.keys());
        this.styleNames.push("")
      }
    })
    this.svgEditService.selectedElement.subscribe({
      next: selected => {
        this.selectedElement = selected
        this.selectedElementStyleName.setValue(svgStyleService.getElementStyleName(selected))
      }
    })
    this.selectedElementStyleName.valueChanges.subscribe({
      next: value => {
        this.svgStyleService.setStyle(this.selectedElement, value)
      }
    })
  }

  ngOnInit(): void {
  }

}
