import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleAssignComponent } from './style-assign.component';

describe('StyleAssignComponent', () => {
  let component: StyleAssignComponent;
  let fixture: ComponentFixture<StyleAssignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StyleAssignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
