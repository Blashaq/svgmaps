import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogDelete } from '@app/dialogs/delete/dialog-delete';
import { SvgTextService } from '@app/services/svg-text/svg-text.service';
import { TextData } from '@app/services/svg-text/textUtils';
import { interval } from 'rxjs';
import { debounce } from 'rxjs/operators';

@Component({
  selector: 'app-text-edit',
  templateUrl: './text-edit.component.html',
  styleUrls: ['./text-edit.component.css']
})
export class TextEditComponent implements OnInit {

  @Input("textElement") textElement: TextData;

  textForm: FormGroup;

  constructor(private svgTextService: SvgTextService,
    private dialog: MatDialog) {
    this.textForm = new FormGroup({
      text: new FormControl('lorem ipsum'),
      x: new FormControl(0),
      y: new FormControl(0),
      font: new FormControl(''),
      fontFill: new FormControl(''),
      fontStroke: new FormControl('')
    })
  }

  deleteText() {
    const dialogRef = this.dialog.open(DialogDelete, {
      width: '250px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.svgTextService.deleteText(this.textElement);
      }
    });

  }


  ngOnInit(): void {
    this.textForm.get("text").setValue(this.textElement.text);
    this.textForm.get("x").setValue(this.textElement.x);
    this.textForm.get("y").setValue(this.textElement.y);
    this.textForm.get("font").setValue(this.textElement.textStyle.font);
    this.textForm.get("fontFill").setValue(this.textElement.textStyle.fontFill);
    this.textForm.get("fontStroke").setValue(this.textElement.textStyle.fontStroke);
    this.textForm.valueChanges.pipe(debounce(() => interval(100))).subscribe({
      next: (form) => {
        this.textElement.x = form.x
        this.textElement.y = form.y
        this.textElement.text = form.text
        this.textElement.textStyle.font = form.font
        this.textElement.textStyle.fontFill = form.fontFill
        this.textElement.textStyle.fontStroke = form.fontStroke

        this.svgTextService.updateText(this.textElement)
      }
    })
  }
}

