import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { EditorStateService, SidebarMode } from '@app/services/editor-state/editor-state.service';

@Component({
  selector: 'app-editor-container',
  templateUrl: './editor-container.component.html',
  styleUrls: ['./editor-container.component.css']
})
export class EditorContainerComponent implements OnInit, AfterViewInit {

  @ViewChild('drawer')
  drawer: MatSidenav;

  sidebarMode: SidebarMode = SidebarMode.styleCreate;

  constructor(private editorStateService: EditorStateService) { }

  isStyleCreatePanel(): boolean {
    return this.sidebarMode === SidebarMode.styleCreate
  }

  isStyleAssignPanel(): boolean {
    return this.sidebarMode === SidebarMode.styleAssign
  }

  isTextEditPanel(): boolean {
    return this.sidebarMode === SidebarMode.addText
  }

  ngOnInit(): void { }

  onClose(): void {
    this.editorStateService.previewStyle(false);
  }

  ngAfterViewInit(): void {
    this.editorStateService.sidebarMenu.subscribe({
      next: value => {
        if (value !== undefined) {
          this.sidebarMode = value;
          this.drawer.toggle(true);
        }
      }
    });
  }
}
