import { Component, OnInit } from '@angular/core';
import { EditorStateService, Mode } from '@app/services/editor-state/editor-state.service';

@Component({
  selector: 'app-menu-edit',
  templateUrl: './menu-edit.component.html',
  styleUrls: ['./menu-edit.component.css']
})
export class MenuEditComponent implements OnInit {

  private isPreview = false;
  private isSvgLoaded = false;
  private isElementSelected = false;

  constructor(private editorState: EditorStateService) { }

  ngOnInit(): void {
    this.editorState.previewMode.subscribe({
      next: value => {
        this.isPreview = value
      }
    })
    this.editorState.svg.subscribe({
      next: svg => {
        if (svg) {
          this.isSvgLoaded = true;
        } else {
          this.isSvgLoaded = false;
        }
      }
    })
    this.editorState.elementSelected.subscribe({
      next: elementSelected => {
        this.isElementSelected = elementSelected;
      }
    })
  }
  isPreviewMode(): boolean {
    return this.isPreview
  }
  canEditPath(): boolean {
    return this.isElementSelected;
  }

  chooseSelect(): void {
    this.editorState.setMode(Mode.select);
    this.editorState.previewStyle(false)
  }

  choosePan(): void {
    this.editorState.setMode(Mode.pan);
  }

  chooseSplit(): void {
    this.editorState.previewStyle(false);
    this.editorState.setMode(Mode.split);
  }

  chooseAddText(): void {
    this.previewStyle();
    this.editorState.openAddText();
  }

  chooseAssignStyle(): void {
    this.previewStyle();
    this.editorState.openAssignStyle();
  }
  chooseCreateStyle(): void {
    this.previewStyle()
    this.editorState.openCreateStyle()
  }

  previewStyle(): void {
    this.editorState.setMode(Mode.pan)
    this.editorState.previewStyle(true);
  }

  editStyle(): void {
    this.editorState.previewStyle(false);
  }

  isFileLoaded(): boolean {
    return this.isSvgLoaded
  }

}
