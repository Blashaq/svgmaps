import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AboutDialogComponent } from '@dialogs/about/about-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'svg-maps';
  constructor(public dialog: MatDialog) { }

  showAbout(): void {
    const dialogRef = this.dialog.open(AboutDialogComponent);
  }
}
