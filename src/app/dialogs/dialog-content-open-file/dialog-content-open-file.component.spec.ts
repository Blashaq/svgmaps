import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogContentOpenFileComponent } from './dialog-content-open-file.component';

describe('DialogContentOpenFileComponent', () => {
  let component: DialogContentOpenFileComponent;
  let fixture: ComponentFixture<DialogContentOpenFileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogContentOpenFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogContentOpenFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
