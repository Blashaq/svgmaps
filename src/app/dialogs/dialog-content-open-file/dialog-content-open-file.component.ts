import { Component, OnInit } from '@angular/core';

export interface OpenFileSelectOption {
  path?: string,
  name: string,
  isUpload?: boolean
}

@Component({
  selector: 'app-dialog-content-open-file',
  templateUrl: './dialog-content-open-file.component.html',
  styleUrls: ['./dialog-content-open-file.component.css']
})
export class DialogContentOpenFileComponent implements OnInit {

  mapList: OpenFileSelectOption[] = [{
    path: '/assets/poland.svg',
    name: 'Poland'
  }, {
    path: '/assets/world.svg',
    name: 'World'
  },
  {
    path: '/assets/spainHigh.svg',
    name: 'Spain'
  },
  {
    name: 'Upload file...',
    isUpload: true
  }];

  selectedMap: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
