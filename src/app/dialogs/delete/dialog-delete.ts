import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'dialog-delete',
    templateUrl: 'dialog-delete.html',
})
export class DialogDelete {
    constructor(public dialogRef: MatDialogRef<DialogDelete>) { }
}