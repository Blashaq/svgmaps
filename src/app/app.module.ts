// ng imports
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppComponent } from '@app/root-app/app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// material imports
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';

// project imports
import { MenuFileComponent } from '@components/menu-file/menu-file.component';
import { WelcomeComponent } from '@components/welcome/welcome.component';
import { EditorWindowComponent } from '@components/editor-window/editor-window.component';
import { AppRoutingModule } from '@misc/app-routing.module';
import { MenuEditComponent } from '@components/menu-edit/menu-edit.component';
import { DialogContentOpenFileComponent } from '@dialogs/dialog-content-open-file/dialog-content-open-file.component';
import { ZoomSliderComponent } from '@components/zoom-slider/zoom-slider.component';
import { EditorContainerComponent } from '@components/editor-container/editor-container.component';
import { AboutDialogComponent } from '@dialogs/about/about-dialog.component';
import { StyleCreateComponent } from '@components/style-create/style-create.component';
import { StyleCreateContainerComponent } from '@components/style-create-container/style-create-container.component';
import { StyleAssignComponent } from '@components/style-assign/style-assign.component';
import { TextEditComponent } from '@components/text-edit/text-edit.component';
import { TextEditContainerComponent } from './components/text-edit-container/text-edit-container.component';
import { DialogDelete } from './dialogs/delete/dialog-delete';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    EditorWindowComponent,
    MenuEditComponent,
    MenuFileComponent,
    DialogContentOpenFileComponent,
    ZoomSliderComponent,
    EditorContainerComponent,
    AboutDialogComponent,
    StyleCreateComponent,
    DialogDelete,
    StyleCreateContainerComponent,
    StyleAssignComponent,
    TextEditComponent,
    TextEditContainerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    AppRoutingModule,
    MatDialogModule,
    MatCardModule,
    MatSnackBarModule,
    HttpClientModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
