import * as SvgPath from 'svgpath';
import { svgNamespace } from '@misc/consts'
import { DrawnPath } from './drawnPath';
import { parseSimpleElementStyle, Selector, getDOMStyles, DOMElementStyle, ElementStyle, createDomStyle, getStyleText, mapPathClass, selectedClass } from '../svg-style/styleUtils';
import { getDomTextElements, parseTextStyle, TextData } from '@services/svg-text/textUtils';
import { CssJs } from 'jotform-css.js';
import { ElementStyleMap } from '@services/svg-style/svg-style.service';


enum AbsolutePathElem {
  start = 'M',
  end = 'Z',
}

export interface ViewBox {
  x: number;
  y: number;
  h: number;
  w: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface PathPoint extends Point {
  distance: number;
  index: number;
  segment: Array<any>;
}

export class EditableSVG {

  viewBox: ViewBox;
  initialViewBox: ViewBox;
  zoom = 1;

  selectedElement: SVGGeometryElement;
  selectedElementAbsPath: typeof SvgPath;
  selectedElementFragments = new Array<SVGGeometryElement>();

  // path sub-element that is being edited
  editedFragment: SVGGeometryElement;
  editedFragmentAbsPath: typeof SvgPath;

  styles: DOMElementStyle[] = []
  texts: TextData[] = []

  constructor(public svgImage: SVGSVGElement, private cssParser: CssJs) {
    this.svgImage.removeAttribute('width');
    this.svgImage.removeAttribute('height');
    this.preprocessStyles();
    this.preprocessText();
  }

  getStyles(): ElementStyle[] {
    return this.styles
  }

  getTexts(): TextData[] {
    return this.texts;
  }

  deleteText(deleted: TextData) {
    this.texts = this.texts.filter(text => deleted.domText !== text.domText);
    deleted.domText.parentElement.removeChild(deleted.domText)
  }

  addText() {

    const newTextElement = document.createElementNS(svgNamespace, 'text') as SVGTextElement;
    const { x, y, width, height } = this.svgImage.getBBox();
    const cx = width / 2 + x;
    const cy = height / 2 + y;
    newTextElement.textContent = 'example text';
    newTextElement.setAttribute('style', 'font:30px roboto;fill:white;stroke:black;');
    newTextElement.setAttribute('x', `${cx}`);
    newTextElement.setAttribute('y', `${cy}`);

    this.svgImage.appendChild(newTextElement);
    this.preprocessText();
  }

  updateStyleDefinition(simpleStyle: ElementStyle) {
    const style = this.styles.find(style => style.name === simpleStyle.name)
    if (style) {
      const styleText = getStyleText(simpleStyle)
      style.domElement.innerHTML = styleText
    }
  }
  deleteStyleDefinition(simpleStyle: ElementStyle) {
    const style = this.styles.find(style => style.name === simpleStyle.name)
    if (style) {
      style.domElement.parentElement.removeChild(style.domElement)
      this.styles = this.styles.filter(style => style.name !== simpleStyle.name)
    }
  }
  createStyleDefinition(simpleStyle: ElementStyle) {
    const domStyle = createDomStyle(simpleStyle, this.svgImage)
    this.styles.push(new DOMElementStyle(simpleStyle, domStyle))
  }

  addEditorStyles(): void {
    const paths = this.svgImage.getElementsByTagName('path');
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < paths.length; i++) {
      const path = paths[i];
      path.classList.add(mapPathClass);
    }
    if (this.selectedElement) {
      this.selectedElement.classList.add(selectedClass);
    }

  }

  removeEditorSelectors(): void {
    const paths = this.svgImage.getElementsByTagName('path');
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < paths.length; i++) {
      const path = paths[i];
      path.classList.remove(mapPathClass);
    }
    if (this.selectedElement) {
      this.selectedElement.classList.remove(selectedClass);
    }

  }

  switchFocus(selected: SVGGeometryElement): SVGGeometryElement {
    if (this.selectedElement !== selected) {
      this.unselect();
      return this.select(selected);

    }
    else {
      this.unselect();
      return undefined;
    }
  }

  select(selected: SVGGeometryElement): SVGGeometryElement {
    this.selectedElement = selected;
    this.selectedElementFragments = this.getPathFragments(this.selectedElement);
    this.selectedElementAbsPath = new SvgPath(selected.getAttribute('d')).abs();
    this.selectedElement.classList.add(selectedClass);
    if (!(this.selectedElement.parentElement instanceof SVGGElement)) {
      const containerElement = document.createElementNS(svgNamespace, 'g') as SVGGElement;
      const parent = this.selectedElement.parentElement;
      const children: Array<ChildNode> = []
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < parent.childNodes.length; i++) {
        children.push(parent.childNodes[i]);
      }

      children.forEach(child => {
        parent.removeChild(child);
        containerElement.appendChild(child);
      });
      containerElement.appendChild(this.selectedElement);
      parent.appendChild(containerElement);
    }

    return this.selectedElement;
  }

  unselect(): void {
    this.selectedElement?.classList.remove(selectedClass);
    this.selectedElement = undefined;
    this.selectedElementFragments = [];
    this.editedFragment = undefined;
    this.editedFragmentAbsPath = undefined;
  }

  getCleanedUpElement(): SVGSVGElement {
    const serializer = new XMLSerializer();
    const parser = new DOMParser();
    const serializedImage = serializer.serializeToString(this.svgImage);
    const copy = parser.parseFromString(serializedImage, 'image/svg+xml').documentElement;
    if (copy instanceof SVGSVGElement) {
      return copy;
    }
    return undefined;
  }

  setInitialViewbox(viewBox: ViewBox) {
    this.initialViewBox = viewBox
    this.setViewBox(viewBox)
  }
  setViewBox(viewBox: ViewBox): void {
    this.viewBox = viewBox;
    this.svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
  }

  pan(start: Point, end: Point, panningFinished: boolean): void {
    const dx = (start.x - end.x) * this.zoom;
    const dy = (start.y - end.y) * this.zoom;
    const viewBox = { x: this.viewBox.x + dx, y: this.viewBox.y + dy, w: this.viewBox.w, h: this.viewBox.h };
    this.svgImage.setAttribute('viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`);
    if (panningFinished) {
      this.setViewBox(viewBox);
    }
  }

  getClickCoordinates(domClickPoint: Point): DOMPoint {
    const point = this.svgImage.createSVGPoint();
    point.x = domClickPoint.x;
    point.y = domClickPoint.y;
    return point.matrixTransform(this.svgImage.getScreenCTM().inverse());
  }

  setZoom(zoom: number): void {
    this.zoom = zoom;
    const viewBox = {
      x: this.viewBox.x,
      y: this.viewBox.y,
      h: this.initialViewBox.h,
      w: this.initialViewBox.w
    };
    viewBox.w = viewBox.w * zoom;
    viewBox.h = viewBox.h * zoom;
    this.setViewBox(viewBox);
  }

  setClickedFragment(point: DOMPoint): void {
    this.selectedElementFragments.forEach(fragment => {
      this.svgImage.appendChild(fragment);
    });

    this.editedFragment = this.selectedElementFragments.find(fragment => fragment.isPointInFill(point));
    this.editedFragmentAbsPath = new SvgPath(this.editedFragment.getAttribute('d')).abs();
    this.selectedElementFragments = this.selectedElementFragments.filter(fragment => fragment !== this.editedFragment);

    this.selectedElementFragments.forEach(fragment => {
      this.svgImage.removeChild(fragment);
    });
    this.svgImage.removeChild(this.editedFragment);
  }

  isInsideEditedFragment(point: DOMPoint): boolean {
    this.svgImage.appendChild(this.editedFragment);
    const result = this.editedFragment.isPointInFill(point);
    this.svgImage.removeChild(this.editedFragment);
    return result;
  }

  splitSelectedPath(line: DrawnPath): void {

    const newElements = this.split(this.editedFragmentAbsPath, line);
    if (this.selectedElementFragments.length > 0) {
      const joinedFragments = this.joinElements(this.selectedElementFragments);
      newElements.push(joinedFragments);

    }
    newElements.forEach(element => {
      element.setAttribute('class', 'mapPath');
      this.selectedElement.parentElement.appendChild(element);
    });


    const oldElementAttributes = this.selectedElement.attributes;

    this.removeEditorSelectors();

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < oldElementAttributes.length; i++) {
      const attr = oldElementAttributes[i];
      if (attr.name === 'id') {
        for (let j = 0; j < newElements.length; j++) {
          newElements[j].setAttribute('id', `${attr.value}${j}`);
        }
      }
      else if (attr.name !== 'd') {
        newElements.forEach(element => element.setAttribute(attr.name, attr.value));
      }
    }
    if (this.selectedElement.parentElement instanceof SVGGElement) {
      reorderChildren(this.selectedElement.parentElement);
    }

    this.selectedElement.parentElement.removeChild(this.selectedElement);
    this.unselect();
    this.addEditorStyles();
  }

  /**
   * Parses def <style> declarations,removes them and makes
   * new <style> element for each found selector.
   */
  private preprocessStyles() {
    const foundStyles = getDOMStyles(this.svgImage)
    const simpleStyles: ElementStyleMap = new Map();
    foundStyles.forEach(style => {
      const text = style.innerHTML;
      const parsed = this.cssParser.parseCSS(text)
      const selectorList: Selector[] = []
      for (let selectorIndex in parsed) {
        const selector: Selector = parsed[selectorIndex]
        selectorList.push(selector)
        const simpleStyle = parseSimpleElementStyle(selector)
        simpleStyles.set(simpleStyle.name, simpleStyle)
      }
    })
    for (let i = 0; i < foundStyles.length; i++) {
      const style = foundStyles[i];
      style.parentElement.removeChild(style)
    }
    simpleStyles.forEach(simpleStyle => {
      const domStyle = createDomStyle(simpleStyle, this.svgImage)
      this.styles.push(new DOMElementStyle(simpleStyle, domStyle))
    })
  }

  private preprocessText() {
    const textsList = getDomTextElements(this.svgImage);
    const processedTextList = textsList.map(domTextElement => {
      const x = domTextElement.getAttribute("x")
      const y = domTextElement.getAttribute("y")
      const styleText = domTextElement.getAttribute("style")
      const text: TextData = {
        x: parseFloat(x),
        y: parseFloat(y),
        textStyle: parseTextStyle(styleText),
        text: domTextElement.textContent,
        domText: domTextElement
      }
      return text
    })
    this.texts = processedTextList;
  }

  private split(absPath: typeof SvgPath, line: DrawnPath): Array<SVGGeometryElement> {
    const el1 = document.createElementNS(svgNamespace, 'path') as SVGGeometryElement;
    const el2 = document.createElementNS(svgNamespace, 'path') as SVGGeometryElement;
    absPath = absPath.abs();

    const { closerToStart, closerToEnd } = findCutPoints(absPath, line);

    const distanceToStart = distance(closerToStart.x, closerToStart.y, line.startP.x, line.startP.y);
    const distanceToEnd = distance(closerToStart.x, closerToStart.y, line.endP.x, line.endP.y);
    const startIsCloser = distanceToStart < distanceToEnd;

    // path 2 starts at the line end that is further away from shape start
    if (startIsCloser) {
      el2.setAttribute('d', `M ${line.endP.x},${line.endP.y}`);
      el2.setAttribute('d', `${el2.getAttribute('d')} ${line.getInvertedD()}`);
    }
    else {
      el2.setAttribute('d', `M ${line.startP.x},${line.startP.y}`);
      el2.setAttribute('d', `${el2.getAttribute('d')} ${line.getD()}`);
    }

    absPath.iterate((segment, index, x, y) => {
      if (index === 0) {
        el1.setAttribute('d', `${formatSegment(segment)}`);
      }
      else {
        // add segments from start to nearest cut point to path 1
        if (index <= closerToStart.index) {
          const d = el1.getAttribute('d');
          el1.setAttribute('d', `${d} ${formatSegment(segment)}`);
        }
        // add segments between cut points to path 2
        if (index >= closerToStart.index && index <= closerToEnd.index) {
          const d = el2.getAttribute('d');
          el2.setAttribute('d', `${d} ${formatSegment(segment)}`);
        }
      }
    });

    // add absolute point of whichever end of the line is closer
    if (startIsCloser) {
      el1.setAttribute('d', `${el1.getAttribute('d')} L ${line.startP.x},${line.startP.y}`);
      el2.setAttribute('d', `${el2.getAttribute('d')} L ${line.endP.x},${line.endP.y} Z`);
    }
    else {
      el1.setAttribute('d', `${el1.getAttribute('d')} L ${line.endP.x},${line.endP.y}`);
      el2.setAttribute('d', `${el2.getAttribute('d')} L ${line.startP.x},${line.startP.y} Z`);
    }

    // insert segments from the line
    if (startIsCloser) {
      el1.setAttribute('d', `${el1.getAttribute('d')} ${line.getD()}`);
    }
    else {
      el1.setAttribute('d', `${el1.getAttribute('d')} ${line.getInvertedD()}`);
    }

    // get remaining absolute end of the line
    if (startIsCloser) {
      el1.setAttribute('d', `${el1.getAttribute('d')} L ${line.endP.x},${line.endP.y}`);
    }
    else {
      el1.setAttribute('d', `${el1.getAttribute('d')} L ${line.startP.x},${line.startP.y}`);
    }

    // insert remaining segments to path 1
    absPath.iterate((segment, index, x, y) => {
      if (index >= closerToEnd.index) {
        const d = el1.getAttribute('d');
        el1.setAttribute('d', `${d} ${formatSegment(segment)}`);
      }
    });
    return [el1, el2];
  }

  private getPathFragments(path: SVGGeometryElement): Array<SVGGeometryElement> {

    const fragmentArray = new Array<SVGGeometryElement>();
    const traversablePath = SvgPath(path.getAttribute('d')).abs();

    let currentDPath = '';

    traversablePath.iterate((segment, index, x, y) => {
      if (segment[0] === AbsolutePathElem.end) {
        currentDPath += `${segment[0]}`;

        const svgFragment = document.createElementNS(svgNamespace, 'path') as SVGGeometryElement;
        svgFragment.setAttribute('d', currentDPath);
        fragmentArray.push(svgFragment);
        currentDPath = '';
      }
      else {
        let nextSegmentString = '';
        segment.forEach(e => {
          nextSegmentString += ` ${e}`;
        });
        currentDPath += nextSegmentString;
      }

    });
    return fragmentArray;
  }

  private joinElements(elements: Array<SVGElement>): SVGGeometryElement {
    const joined = document.createElementNS(svgNamespace, 'path') as SVGGeometryElement;

    const joinedD = elements.map(element => element.getAttribute('d'))
      .reduce((prev, current) => {
        return prev + current;
      });
    joined.setAttribute('d', joinedD);
    return joined;
  }

}

function distance(x1: number, y1: number, x2: number, y2: number): number {
  return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}


function findCutPoints(traversablePath: typeof SvgPath, line: DrawnPath)
  : { closerToStart: PathPoint, closerToEnd: PathPoint } {
  const closestToLineStart = {
    distance: Number.POSITIVE_INFINITY,
    x: 0,
    y: 0,
    segment: [],
    index: 0
  };

  const closestToLineEnd = {
    distance: Number.POSITIVE_INFINITY,
    x: 0,
    y: 0,
    segment: [],
    index: 0
  };

  traversablePath.iterate((segment, index, x, y) => {
    if (index > 0) {
      const distToStart = distance(x, y, line.startP.x, line.startP.y);
      const distToEnd = distance(x, y, line.endP.x, line.endP.y);
      if (distToStart < closestToLineStart.distance) {
        closestToLineStart.distance = distToStart;
        closestToLineStart.x = x;
        closestToLineStart.y = y;
        closestToLineStart.index = index;
      }
      if (distToEnd < closestToLineEnd.distance) {
        closestToLineEnd.distance = distToEnd;
        closestToLineEnd.x = x;
        closestToLineEnd.y = y;
        closestToLineEnd.index = index;
      }
    }
  });

  // node that is closer to M element
  const closerToStart = closestToLineStart.index < closestToLineEnd.index ? closestToLineStart : closestToLineEnd;
  const closerToEnd = closestToLineStart.index < closestToLineEnd.index ? closestToLineEnd : closestToLineStart;

  return { closerToStart, closerToEnd };
}

function formatSegment(segment: any[]): string {
  return segment[0] + segment.slice(1, segment.length).join(',');
}

function reorderChildren(container: SVGGElement): void {
  const textElements = container.getElementsByTagName('text');
  const textElemOrdered: Array<SVGTextElement> = [];
  // tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < textElements.length; i++) {
    textElemOrdered.push(textElements[i]);
  }
  textElemOrdered.forEach(text => {
    container.removeChild(text);
    container.appendChild(text);
  });
}