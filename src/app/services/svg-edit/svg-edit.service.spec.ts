import { TestBed } from '@angular/core/testing';

import { SvgEditService } from './svg-edit.service';

describe('SvgEditService', () => {
  let service: SvgEditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SvgEditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
