import { Injectable } from '@angular/core';
import { DrawnPath } from '@app/services/svg-edit/drawnPath';
import { EditableSVG, Point, ViewBox } from '@services/svg-edit/editableSVG';
import { snackMessages } from '@app/misc/consts';
import { ReplaySubject } from 'rxjs';
import { ElementStyle } from '../svg-style/styleUtils';
import { SvgFileService } from '../svg-file/svg-file.service';
import { TextData } from '@services/svg-text/textUtils';


@Injectable({
    providedIn: 'root'
})
export class SvgEditService {
    private _editedFile: EditableSVG;

    selectedElement = new ReplaySubject<SVGGeometryElement>(1);
    editedFile: ReplaySubject<EditableSVG> = new ReplaySubject(1);
    svgStyles: ReplaySubject<ElementStyle[]> = new ReplaySubject(1);
    svgTexts: ReplaySubject<TextData[]> = new ReplaySubject(1);

    private line: DrawnPath

    constructor(private svgFileService: SvgFileService) {
        this.svgFileService.editableSvg.subscribe({
            next: svg => {
                this._editedFile = svg;
                this.svgStyles.next(svg.getStyles());
                this.svgTexts.next(svg.getTexts());
                this.editedFile.next(this._editedFile);
            }
        })
    }

    editorFunctions = new class {
        constructor(public superThis: SvgEditService) {
        }
        getClickCoordinates(domClickPoint: Point): DOMPoint {
            return this.superThis._editedFile.getClickCoordinates(domClickPoint)
        }
        drawLine(point: DOMPoint): { success: boolean, err?: string } {
            if (!this.superThis._editedFile.selectedElement.isPointInFill(point)) {
                return { success: false, err: snackMessages.clickInsidePlease }
            }
            if (!this.superThis.line) {
                const line = new DrawnPath(this.superThis._editedFile.svgImage, point);
                this.superThis._editedFile.svgImage.appendChild(line.path);
                this.superThis._editedFile.setClickedFragment(point);
                this.superThis.line = line
                return { success: true }
            }
            if (!this.superThis._editedFile.isInsideEditedFragment(point)) {
                return { success: false, err: snackMessages.clickInsidePlease }
            }
            this.superThis.line.addPoint(point)
            return { success: true }

        }
        selectElement(clickedElement: SVGGeometryElement) {
            this.removeLine()
            const selected = this.superThis._editedFile.switchFocus(clickedElement);
            this.superThis.selectedElement.next(selected)
            return selected
        }
        unselectElement() {
            this.removeLine()
            this.superThis._editedFile.unselect()
        }
        splitSelectedPath() {
            this.superThis._editedFile.splitSelectedPath(this.superThis.line)
            this.removeLine()
        }
        removeLine() {
            if (this.superThis.line) {
                this.superThis._editedFile.svgImage.removeChild(this.superThis.line.path)
                this.superThis.line = undefined;
            }
        }
        updateStyleDefinition(style: ElementStyle) {
            this.superThis._editedFile.updateStyleDefinition(style)
            this.superThis.svgStyles.next(this.superThis._editedFile.getStyles())
        }
        deleteStyleDefinition(style: ElementStyle) {
            this.superThis._editedFile.deleteStyleDefinition(style)
            this.superThis.svgStyles.next(this.superThis._editedFile.getStyles())
        }
        createStyleDefinition(style: ElementStyle) {
            this.superThis._editedFile.createStyleDefinition(style)
            this.superThis.svgStyles.next(this.superThis._editedFile.getStyles())
        }
        deleteText(text: TextData) {
            this.superThis._editedFile.deleteText(text);
            this.superThis.svgTexts.next(this.superThis._editedFile.getTexts())
        }
        addText() {
            this.superThis._editedFile.addText();
            this.superThis.svgTexts.next(this.superThis._editedFile.getTexts())
        }
    }(this)

    display = new class {
        constructor(public superThis: SvgEditService) {
        }
        pan(start: Point, end: Point, panningFinished: boolean): void {
            this.superThis._editedFile.pan(start, end, panningFinished)
        }
        setInitialViewbox(vb: ViewBox) {
            this.superThis._editedFile.setInitialViewbox(vb)
        }
        setZoom(value: number) {
            if (this.superThis._editedFile) {
                this.superThis._editedFile.setZoom(value)
            }
        }
        previewStyle() {
            if (this.superThis._editedFile) {
                this.superThis._editedFile.removeEditorSelectors()
            }

        }
        editStyle() {
            if (this.superThis._editedFile) {
                this.superThis._editedFile.addEditorStyles()
            }
        }
    }(this)
}