import * as svgpath from 'svgpath';

const lineBeingDrawnClass = 'line';

export class DrawnPath {

    startP: DOMPoint;
    endP: DOMPoint;
    path: SVGGeometryElement;
    private NS: string;

    constructor(private parentSVG: SVGSVGElement, startPoint: DOMPoint) {
        this.NS = this.parentSVG.getAttribute('xmlns');
        this.path = document.createElementNS(this.NS, 'path') as SVGGeometryElement;
        this.path.classList.add(lineBeingDrawnClass);
        this.startP = startPoint;
        this.addToD(`M${startPoint.x},${startPoint.y}`);
    }

    addPoint(point: DOMPoint): void {
        this.endP = point;
        this.addToD(`L ${point.x} ${point.y}`);
    }

    addToD(value: string): void {
        const d = this.path.getAttribute('d');
        if (d) {
            this.path.setAttribute('d', `${d} ${value}`);
        }
        else {
            this.path.setAttribute('d', `${value}`);
        }
    }

    getD(): string {
        let resultD = '';
        const d = this.path.getAttribute('d');

        const traversablePath = svgpath(d);
        traversablePath.iterate((segment, index, x, y) => {
            if (index > 0) {
                resultD += `${segment[0]} ${segment[1]},${segment[2]}`;
            }
        });
        return resultD;
    }

    getInvertedD(): string {
        let nodes: any[] = [];
        let traversablePath = svgpath(this.path.getAttribute('d'));
        traversablePath = traversablePath.abs();
        traversablePath.iterate((segment, index, x, y) => {
            nodes.push(segment);
        });

        const first = nodes[nodes.length - 1];

        let d = `L${first[1]}, ${first[2]}`;
        nodes = nodes.slice(0, nodes.length - 1);
        const revertedNodes = nodes.reverse();
        revertedNodes.forEach(node => {
            d += `L ${node[1]} ${node[2]}`
        });

        return d;
    }
}
