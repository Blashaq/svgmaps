import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { SvgEditService } from '../svg-edit/svg-edit.service';
import { map } from 'rxjs/operators';
import { ElementStyle, mapPathClass, selectedClass } from './styleUtils';



@Injectable({
  providedIn: 'root'
})
export class SvgStyleService {
  styles = new ReplaySubject<ElementStyleMap>(1)
  private _styles: ElementStyleMap = new Map()
  constructor(private svgEditService: SvgEditService) {
    this.svgEditService.svgStyles.pipe(map(svgStyles => { return this.mapSvgStyles(svgStyles) })).subscribe({
      next: styles => {
        this._styles = styles
        this.styles.next(this._styles)
      }
    })
  }
  updateStyle(style: ElementStyle) {
    this.svgEditService.editorFunctions.updateStyleDefinition(style);
  }
  saveStyle(style: ElementStyle) {
    this.svgEditService.editorFunctions.createStyleDefinition(style);
  }
  hasStyle(name: string) {
    return this._styles.has(name)
  }

  getElementStyleName(element: SVGGeometryElement) {
    let elementClassList = Array.from(element.classList)
    elementClassList = elementClassList.filter(className => className !== selectedClass && className !== mapPathClass)
    if (elementClassList.length > 0) {
      return elementClassList[0];
    }
    return '';
  }
  setStyle(element: SVGGeometryElement, styleName: string) {
    element.setAttribute("class", "")
    if(styleName!==""){
      element.classList.add(styleName)
    }
  }

  delete(style: ElementStyle) {
    this.svgEditService.editorFunctions.deleteStyleDefinition(style)
  }
  mapSvgStyles(styles: ElementStyle[]) {
    let styleMap: ElementStyleMap = new Map();

    styles.forEach(style => {
      styleMap.set(style.name, style);
    })
    return styleMap
  }
}

export type ElementStyleMap = Map<string, ElementStyle>

