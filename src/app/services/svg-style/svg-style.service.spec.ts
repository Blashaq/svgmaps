import { TestBed } from '@angular/core/testing';

import { SvgStyleService } from './svg-style.service';

describe('SvgStyleService', () => {
  let service: SvgStyleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SvgStyleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
