import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { EditableSVG } from '../svg-edit/editableSVG';
import { SvgFileService } from '../svg-file/svg-file.service';

export enum Mode {
  select = 1,
  pan = 2,
  split = 3,
}

export enum SidebarMode {
  styleCreate = 1,
  styleAssign = 2,
  addText = 3
}

@Injectable({
  providedIn: 'root'
})
export class EditorStateService {
  svg: ReplaySubject<EditableSVG>;
  mode = new BehaviorSubject(Mode.pan);
  scale = new BehaviorSubject(1);
  elementSelected = new BehaviorSubject(false)

  previewMode = new BehaviorSubject(true);
  sidebarMenu = new BehaviorSubject<SidebarMode>(undefined);


  constructor(private svgFileService: SvgFileService) {
    this.svg = this.svgFileService.editableSvg
    this.svgFileService.editableSvg.subscribe({
      next: svg => {
        this.reset()
      }
    })

  }

  previewStyle(isPreview: boolean): void {
    this.previewMode.next(isPreview);
  }

  setZoom(zoom: number): void {
    this.scale.next(zoom);
  }
  setMode(mode: Mode): void {
    this.mode.next(mode);
  }

  openAddText(): void {
    this.sidebarMenu.next(SidebarMode.addText);
  }
  openAssignStyle(): void {
    this.sidebarMenu.next(SidebarMode.styleAssign);
  }
  openCreateStyle(): void {
    this.sidebarMenu.next(SidebarMode.styleCreate)
  }

  reset(): void {
    this.setMode(Mode.pan);
    this.setZoom(1);
    this.previewStyle(true);
    this.elementSelected.next(false);
  }
}
