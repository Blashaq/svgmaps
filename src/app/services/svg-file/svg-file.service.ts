import * as cssjs from 'jotform-css.js';
import { Injectable } from '@angular/core';
import { EditableSVG } from '@services/svg-edit/editableSVG';
import { ReplaySubject } from 'rxjs';

export const incorrectFileTypeMessage = `Incorrect file type`

@Injectable({
    providedIn: 'root'
})
export class SvgFileService {

    private cssParser = new cssjs.cssjs();
    private svgString: string;
    private serializer = new XMLSerializer();

    private _editableSvg: EditableSVG;
    editableSvg = new ReplaySubject<EditableSVG>(1);

    async loadSvg(fileBuffer: ArrayBuffer): Promise<void> {
        const decoder = new TextDecoder();
        this.svgString = decoder.decode(fileBuffer);
        const parser = new DOMParser();
        const elem = parser.parseFromString(this.svgString, 'image/svg+xml').documentElement;
        if (elem instanceof SVGSVGElement) {
            this._editableSvg = new EditableSVG(elem, this.cssParser);

            this.editableSvg.next(this._editableSvg);
        }
        else {
            throw new Error(incorrectFileTypeMessage);
        }
    }

    getSerializedSvg(): string {
        const cleanedUpElement = this._editableSvg.getCleanedUpElement();
        return this.serializer.serializeToString(cleanedUpElement);
    }
}
