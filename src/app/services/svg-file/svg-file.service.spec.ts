import { incorrectFileTypeMessage, SvgFileService } from './svg-file.service';
import * as rxjs from 'rxjs';
import { EditorStateService } from '../editor-state/editor-state.service';
import { SvgEditService } from '../svg-edit/svg-edit.service';

const enc = new TextEncoder()

const svg = `<svg xmlns="http://www.w3.org/2000/svg" baseprofile="tiny" fill="#7c7c7c" height="948" stroke="#ffffff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" version="1.2" width="1000">
 <path d="M866.3 77.7l0.3 1.3-2.5 8.3-5.7 6-6.6 4.5-22.2 3.1-9.4 z" id="POL3139" name="Warmian-Masurian">
 </path>
</svg>`
const svgBuffer = enc.encode(svg)
const svgDom = new DOMParser().parseFromString(svg, 'image/svg+xml').documentElement as unknown as SVGSVGElement

let editorStateSpy: EditorStateService

describe('SvgfileService', () => {
  let service: SvgFileService;

  let svgEditServiceSpy = jasmine.createSpyObj<SvgEditService>('SvgEditService', { 'getCleanedUpElement': svgDom, 'next': undefined })
  svgEditServiceSpy


  beforeEach(() => {
    service = new SvgFileService(editorStateSpy, svgEditServiceSpy);
    editorStateSpy = jasmine.createSpyObj<EditorStateService>('EditorStateService', { 'reset': undefined }, { 'svg': new rxjs.ReplaySubject(1) })
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should fail on empty buffer load', async () => {
    const emptyBuffer = new ArrayBuffer(10)
    await expectAsync(service.loadSvg(emptyBuffer)).toBeRejectedWithError(incorrectFileTypeMessage)
  })
  it('should trigger next() on svg stream on load', async () => {
    await expectAsync(service.loadSvg(svgBuffer)).toBeResolved()
    editorStateSpy.svg.subscribe(val=>{
      expect(val).toBeTruthy()      
    })
  })
  it('should return serialized svg ', async () => {
    await service.loadSvg(svgBuffer)
    const serialized = service.getSerializedSvg()
    expect(typeof serialized).toBe('string')
  })
});
