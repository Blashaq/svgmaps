import { svgNamespace } from "@app/misc/consts";
import { colourNameToHex } from "../svg-style/styleUtils";

export interface TextData {
    x: number;
    y: number;
    textStyle: TextStyle;
    text: string;
    domText?: SVGElement
}

export interface TextStyle {
    font: string;
    fontFill: string;
    fontStroke: string;
}

export function getDomTextElements(svg: SVGSVGElement) {
    const textsList = svg.getElementsByTagNameNS(svgNamespace, "text");
    return Array.from(textsList);
}

export function parseTextStyle(inlineCss: string): TextStyle {

    const style = new Map(inlineCss.split(";").filter(segment => segment.includes(":")).map(segment => {
        const [key, val] = segment.split(":");
        return [key.trim(), val.trim()];
    }))
    const textStyle: TextStyle = {
        font: style.get("font"),
        fontFill: colourNameToHex(style.get("fill")),
        fontStroke: colourNameToHex(style.get("stroke"))
    }
    return textStyle
}