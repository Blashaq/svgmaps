import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { SvgEditService } from '../svg-edit/svg-edit.service';
import { TextData } from './textUtils';

@Injectable({
  providedIn: 'root'
})
export class SvgTextService {

  private _textElements: TextData[];
  textElements: ReplaySubject<TextData[]> = new ReplaySubject(1);

  constructor(private svgEditService: SvgEditService) {
    this.svgEditService.svgTexts.subscribe({
      next: textElements => {
        this._textElements = textElements;
        this.textElements.next(this._textElements)
      }
    })
  }
  //font:30px roboto;fill:white;stroke:white;
  updateText(text: TextData) {
    const styleText = `font:${text.textStyle.font}; fill:${text.textStyle.fontFill}; stroke:${text.textStyle.fontStroke}`
    text.domText.setAttribute("x", `${text.x}`);
    text.domText.setAttribute("y", `${text.y}`);
    text.domText.setAttribute("style", styleText);

    text.domText.textContent = text.text
  }
  deleteText(text: TextData) {
    this.svgEditService.editorFunctions.deleteText(text)
  }

  addText() {
    this.svgEditService.editorFunctions.addText();
  }
}
