import { TestBed } from '@angular/core/testing';

import { SvgTextService } from './svg-text.service';

describe('SvgTextService', () => {
  let service: SvgTextService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SvgTextService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
