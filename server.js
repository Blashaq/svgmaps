const express = require('express')
const app = express()

const port = process.env.PORT || 4200

app.use(express.static(`./dist/svg-maps`));

app.get(`/*`, function (req, res) {
    res.sendFile(`index.html`, { root: `dist/svg-maps/` }
    );
});

app.listen(port, () => {
    console.log(`svgmaps listening at http://localhost:${port}`)
})